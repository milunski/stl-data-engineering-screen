# STL Data Engineering Technical Interview Guide

This is the technical screen for candidates looking to work for STL Data Engineering. This screening is meant to assess the technical fit with our Slalom STL team, and it does not cover assessing consulting/client-facing skills or team dynamics.

## Language Prereqs

At a minimum, candidates are required to have SQL and Python in their toolbox as
the vast majority of data applications treat Python and SQL as first-class languages. The Python requirement may be waved if and only if the candidate has demonstrated experience in another GPL commonly used in developing data infrastructure or interacting with data applications (e.g., JavaScript/TypeScript, Golang, Scala, Java, C#/.NET). In the event of this happening, the candidate can use one of these languages to complete the technical screen.

The SQL requirement is non-negotiable.

## Knowledge

Technologies are always changing, so it is more important to focus on the underlying theory of frameworks as the theory stays relatively constant. The following can be treated as a picklist that is up to the interviewers discretion to cover, with asterisked items being ones that are fundamental to Consultant and higher:

- Datastores
  + OLAP and OLTP style databases *
  + Object storage *
  + Document/Graph DBs
- Data Processing
  + Batch and streaming *
  + Message queues
  + MapReduce-based frameworks *
  + Change Data Capture
- Data Warehousing
  + Snowflake and Star Schemas *
  + Dimensional modeling *
  + [Functional data warehousing](https://maximebeauchemin.medium.com/functional-data-engineering-a-modern-paradigm-for-batch-data-processing-2327ec32c42a)
- API Design
- Data Structures *
- Algorithms
- Security best practices *
- Networking


## Python & SQL Challenge Ratings

Please add your challenge to the table below and give it a ranking.

| Directory Path | Name | Language | Rating (Beginner/Intermediate/Advanced)|
| ---------------| ---- |  ------- | ------ |
| `python/fizz_buzz` | Fizz Buzz | Python | Beginner |
| `python/calculate_median` | Calculate Median | Python | Beginner-Intermediate |

## Contributions

Please contribute! There's plenty of work to be done here, but many hands will make light of it. If you would like to contribute, please reach out to me via email or Teams.
